---
title: Pelican E Perchè Generare Siti Statici
date: 2018-04-20 00:00:00 +0000
slug: pelican-e-perche-generare-siti-statici
summary: Oggi nel web tutti hanno un sito o un blog che aggiornano e mantengono con
  articoli e informazioni di vario genere ed esistono svariate piattaforme per siti
  web dinamici.
convention:
  date: 2018-04-20 00:00:00 +0000
  hour: '11:00'
  event: Pycon9
  speakerdeck_code: 9bbc8ba0169242c1ac7c355adebca67e
  source: https://github.com/fundor333/pelican-e-perche-generare-siti-statici

---
Oggi nel web tutti hanno un sito o un blog che aggiornano e mantengono con articoli e informazioni di vario genere ed esistono svariate piattaforme per siti web dinamici.

Qui invece si parla del contrario, di come mantenere un sito statico, più performante e che risulta anche più economico e rilassante da mantenere, integrandolo con tecnologie come i Jupyter Notebook e Git per ottenere una completa interazione con le tecnologie più moderne e usate. Verranno anche presentati un paio di casi d’uso di Pelican in situazioni reali.

Come prerequisiti si assume che l’ascoltatore sappia cosa sia un CMS e (a grandi linee) come funziona e cosa sia un Jupyter Notebook (non è necessario che sappia usarne uno)


{{< youtube Z8t0lNEmnHQ >}}
