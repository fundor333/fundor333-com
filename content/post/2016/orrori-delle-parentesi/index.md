---
title: Orrori delle parentesi
date: 2016-11-09 00:00:00 +0000
tags:
- programmazione
- rant
slug: orrori-delle-parentesi
description: Digressione sull'uso delle parentesi nei linguaggi di programmazione
categories:
- rant
aliases:
- "/blog/orrori-delle-parentesi/"

feature_image: "orrori_della_parentesi.jpg"
feature_link: "https://unsplash.com/photos/XMpXzzWrJ6g"
feature_text: "Photo by Annie Spratt on Unsplash"

---
(°,,,°)

La mia faccia quando ho aperto twitter...

Un frammento di codice con parentesi messe allineate in fondo...

<!--more-->

{{< tweet 788690145822306304 >}}

\\|/(x,,,x)\\|/

Questo è, appunto, uno di quei limiti da non superare. Il *bon ton*
della programmazione ti dovrebbe mostrare come, quando sviluppi il
codice, questo rimanga leggibile anche a distanza di anni e con persone
diverse, non creare orrori tipo questo in cui non ci si rende conto
velocemente di quale parentesi chiude quale perchè, QUANDO rimetterai
mano al codice (succederà di sicuro) ti ritroverai a imprecare e
maledirti per quello che hai fatto.

Quindi pensa sempre a come scrivi il tuo codice, perchè ti tornerà
indietro sotto forma di karma, tanto karma.
