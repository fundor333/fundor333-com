---
title:  L'inizio di una avventura
date: 2016-09-26 00:00:00 +0000
tags:
- aggiornamento
- blog
- fundor333
slug: linizio-di-una-avventura
description: L' inizio di un blog è sempre difficile. Soprattutto se è la 4-5 volta che riprovi a scriverne uno
categories:
- rant
aliases:
- "/blog/orrori-delle-parentesi/"

feature_image: "inizio-di-una-avventura.jpg"
feature_link: "https://unsplash.com/photos/CJr_tPKpieA"
feature_text: "Photo by Justin Lawrence on Unsplash"

---

L' inizio di un blog è sempre difficile. Soprattutto se è la 4-5 volta
che riprovi a scriverne uno. E ancora di più se rimani venti minuti a
fissare il bianco sullo schermo aspettando che appaia quello che vuoi
scrivere.

Ma questo non sarà un blog sul mio blocco dello scrittore ma sarà la mia
vetrina. Qui mostrerò i miei interessi informatici, le mie opinioni
sulla tecnologia e alcune esperienze vissute riparando pc e altri
lavoretti fatti da informatico freelance.

Per quanto mi riguarda sono un uomo, informatico, pythonista e, da
alcuni, definito anche nerd. Nel mentre scrivo questo post devo dare
solo l' ultimo esame di informatica pur avendo finito la tesi (per
assurdo manca solo il titolo). Molto interessato a docker,python, open
source e hack vari intendi portare questi sul sito.

La cadenza di aggiornamento di questo blog? Non ne ho idea. Penso
settimanale ma questo potrebbe non essere vero. Di sicuro presto ci sarà
un altro post.
