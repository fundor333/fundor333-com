---
title: DataBeers VCE 1
talk: Tre presentazioni da speacker di vario tipo
slot:
  start: 2017-02-17 00:00:00 +0000
  end: 2017-02-17 00:00:00 +0000
  url: https://databeersvce.tumblr.com/post/155615836590/databeers-venezia-1-170217-negozio-piave-67
  group:
  - DataBeers Venezia
  location: Negozio Piave 67, Via Piave 67, Venezia

---
Hey Data Lovers! The wait is over! The first Databeers Venezia is coming! Save the date! Friday, February 17th, 2017 at Negozio Piave 67 (just few minutes walking from Venezia-Mestre railway station). For the first event we have great speakers for you!

Alessandro Scarpellini
Aesse branding & visual communication | Rimini

Valentino Pediroda
CEO and Founder of ModeFinance, the first fintech Credit Rating Agency in Europe | Trieste

Duccio Schiavon
CEO at Quantitas, Data Visualization | Venezia

We can’t wait to see all of you there, but please REGISTER and CONFIRM your attendance, so we’ll be sure to provide enough beers for you! Many thanks for the support to “Negozio Piave 67” and LARUS.

Beers are VERY KINDLY sponsored by Estrella!

Stay tuned, we’re going to share more updates here and on our official Twitter account!

Alice, Elisa, Fabio, Marco, Rocco
