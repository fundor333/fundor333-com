---
title: Convention ed eventi

---
Questo è un elenco completo dei talk e dei convegni a cui ho partecipato nel corso degli anni e a quelli che ho intenzione di partecipare. Tutto questo viene anche inserito in un calendario scaricabile/abbonabile nel classco formato _.ics_ al [link](https://www.fundor333.com/event/index.ics).
