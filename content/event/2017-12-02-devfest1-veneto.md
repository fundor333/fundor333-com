---
title: GDG Venezia Tech Talks @ Ca' Foscari
talk: Le DevFest sono delle vere e proprie 'feste degli sviluppatori' organizzate
  dai GDG di tutto il mondo
slot:
  start: 2017-12-02 00:00:00 +0000
  end: 2017-12-02 00:00:00 +0000
  group:
  - GDG Venezia
  url: https://gdg-venezia.github.io/devfest1-veneto/
  location: Fabrica, Via Postioma 54/F, Catena di Villorba - Treviso

---
Le DevFest sono delle vere e proprie "feste degli sviluppatori" organizzate dai GDG di tutto il mondo, nel periodo tra settembre e novembre. Sono degli eventi composti da talk, codelab e momenti di networking dedicati a sviluppatori e designer professionisti, ad imprenditori, studenti o semplici appassionati e curiosi. Si tratta di un'occasione per condividere insieme competenze e passioni all'interno dell'universo delle tecnologie Google.
