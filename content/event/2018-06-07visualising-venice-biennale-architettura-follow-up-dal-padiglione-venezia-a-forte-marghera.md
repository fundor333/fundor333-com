---
title: >-
  Visualising Venice: Biennale Architettura Follow UP! Dal Padiglione Venezia a
  Forte Marghera
date: 2018-06-07T16:25:00.000Z
talk: Follow UP
slot:
  end:  2018-06-07T16:25:00.000Z
  group: []
  location: Forte Marghera
  start:  2018-06-07T16:25:00.000Z
  url: >-
    https://www.eventbrite.it/e/biglietti-visualising-venice-biennale-architettura-follow-up-dal-padiglione-venezia-al-forte-marghera-46694717156#
---
# Secondo incontro del Padiglione Venezia a Forte Marghera

Nelle aule seminariali - appena entrati in fondo sulla destra - incontreremo il gruppo di ricerca Visualising Venice, è un'iniziativa di Digital Humanities di studenti, studiosi e architetti con progetti di ricerca per generare modelli digitali e mappe della città di Venezia, dei suoi territori e della sua laguna. Parleremo dei casi di studio presentati al Padiglione Venezia e delle attività di raccolta, analisi e riproduzione della Città dai documenti storici.

Ne parleramo con:

* Caroline Bruzelius (Duke University)
* Donatella Calabi (IUAV)
* Andrea Giordano (Università degli studi di Padova)
* Moderatore Luca Corsato, staff curatoriale Padiglione Venezia (OSD)
