---
title: DataBeers Tuscany 3
talk: Birre e presentazioni di dati in Toscana
slot:
  start: 2017-04-06 00:00:00 +0000
  end: 2017-04-06 00:00:00 +0000
  url: https://databeerstus.tumblr.com
  group:
  - PyData Italia
  - Pycon Italy
  location: Pycon8 c/o Grand Hotel Mediterraneo, Lungarno del Tempio 44, Firenze

---
Lo spirito dei DataBeers è quello di dare la possibilità a appassionati, smanettoni e professionisti dei dati di raccontare quello che hanno scoperto su un data base a loro disposizione, come hanno impiegato una certa API, come hanno impiegato in modo "smart" i dati per risolvere un problema, cosa hanno scoperto analizzando twitter .... e molto altro!

DATABEERS TUSCANY

Per la terza edizione di Databeers Tuscany, in collaborazione con PyData Italy e organizzeremo un evento all'interno della PyCon8, la conferenza italiana che ogni anno accoglie centinaia di Python Lover a Firenze.

Per questa edizione la birra sarà offerta da Estrella :-)

Estrella

Anche questa volta potrai assistere alla presentazione di 5 Data Case della durata di 12 minuti ciascuno (7 minuti di presentazione + 5 minuti di domande). E se non ami la matematica o la statistica non preoccuparti... nelle presentazioni è vietato agli speaker l'utilizzo di formule ;-) .

Il programma del III° Data Beers Tuscany è il seguente:

ore 19.00: check-in iscritti

ore 19.30: Benvenuto dagli orgaizzatori e presentazione dei talk

    Smartcity vs Big Data vs IoT, Paolo Nesi
    Artistic Side of Spatial Data, Michele Ferretti
    Analisi Evolutiva del Calciomercato. Stefano Mastini
    Fast Retail Big Data Analysis, Valerio Maggio e Ernesto Arbitrio.
    TBA

ore 20.45: Networking and Chatting

Se oltre partecipare come spettatore vuoi presentare un Data Case scrivi a databeers.tuscany [ \at] gmail.com.

Ci vediamo al prossimo DataBeers!

Nel frattempo seguici su twitter, unisci al nostro gruppo facebook e partecipa alle nostre conversazioni
