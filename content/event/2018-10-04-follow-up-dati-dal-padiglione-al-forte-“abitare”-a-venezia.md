---
title: 'Follow UP! Dati dal Padiglione al Forte: “Abitare” a Venezia'
date: 2018-10-04T16:00:00.000Z
talk: Follow UP!
slot:
  end: 2018-10-04T20:39:42.929Z
  group: []
  location: Forte Marghera
  start: 2018-10-04T20:39:42.926Z
  url: >-
    https://www.eventbrite.it/e/biglietti-follow-up-dati-dal-padiglione-al-forte-abitare-a-venezia-50389110185#
---
Descrizione
Matteo Basso, Federica Fava, Laura Fregolent, Renato Gibin presentano:

Venezia verrà raccontata a partire da alcune ricerche condotte all’interno del Laboratorio di Analisi urbana e del territorio e del ClusterLab: H-City Housing in the city. Abitare e rigenerare, dell’Università IUAV di Venezia. La sovrapposizione dei risultati di diverse indagini condotte sui temi casa, commercio, uso ai piani terra e territori di margine, consentirà di restituire realtà e dinamiche di una città in veloce e continua trasformazione.

Dati

* [Mappa dell'abbandono](http://iuav-labgis.maps.arcgis.com/apps/MapJournal/index.html?appid=c80b12379b7c4c119fed69d2ad3845f3)
* [Quali case per quali abitanti?](http://iuav-labgis.maps.arcgis.com/apps/MapJournal/index.html?appid=83e4a30bba954268ada8d1b5f7858701)
