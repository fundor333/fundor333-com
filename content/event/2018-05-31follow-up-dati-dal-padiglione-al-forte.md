---
title: Follow UP! Dati dal Padiglione al Forte
date: 2018-05-31T16:30:50.816Z
talk: Follow Up
slot:
  end: 2018-05-30T22:00:00.000Z
  group: []
  location: Forte Marghera
  start: 2018-05-31T20:48:50.824Z
  url: >-
    https://www.eventbrite.it/e/biglietti-follow-up-dati-dal-padiglione-al-forte-46493305729#
---
OSD opensensorsdata illustrerà il modello di gestione dati del Padiglione Venezia assieme a DataBeers Venezia, gli analisti dati con la schiuma, e Hacks/Hackers Venezia, gli smanettoni e i parolai con i dati tra giornalisti e hacker.
