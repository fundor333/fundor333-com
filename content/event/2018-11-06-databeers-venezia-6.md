---
title: 'DataBeers Venezia #6'
date: 2018-11-06T17:30:00.000Z
talk: DataBeers
slot:
  end: 2018-11-06T17:30:00.000Z
  group: []
  location: Negozio Piave 67
  start: 2018-11-06T17:30:00.000Z
  url: 'https://www.eventbrite.com/e/databeers-venezia-6-tickets-51764979447#'
---
DataBeers Venezia #6 - 30.11.18 @ Negozio Piave 67, Venezia



We are back again, data lovers from the Lagoon!



Great speakers for you next Friday November 30 @ Negozio Piave 67, Mestre, Venezia, starting at 18:30 sharp!



Tonight we have the pleasure to be with:



**Lorenzo Brutti @lbrutti
**

Data lover indipendente - Mogliano Veneto

Datawalking: passeggiare nel datascape 



**Silvia Pavan 
**

Lead Analyst at FIS - Vicenza

La natura dà i numeri? 



**Francesco Costa @francescocosta
**

Vicedirettore @ilpost - Milano

Gli Stati Uniti, i dati e le farfalle



\---


Please REGISTER as soon as possible so we can provide enough beers (and bagigi) for you all!


Beers are again VERY KINDLY PROVIDED BY ESTRELLA!

Thanks to LARUS for the technical support and logistics.



See you there!


Cheers



Alice, Fabio, Francesco, Lorenzo, Marco, Nicola, Rocco
