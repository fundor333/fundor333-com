---
title: DataBeers VCE 3
talk: Tre presentazioni da speacker di vario tipo
slot:
  start: 2017-11-23 00:00:00 +0000
  end: 2017-11-23 00:00:00 +0000
  url: https://databeersvce.tumblr.com/post/166982206315/databeers-venezia-3-23112017-negozio-piave
  group:
  - DataBeers Venezia
  location: Negozio Piave 67, Via Piave 67, Venezia

---
Hey Data and Beers lovers! We’re back! We are launching the third Databeers Venezia! We are waiting for you next Thursday November 23 @ Negozio Piave 67, Venezia, starting at 19:30 sharp!

It will be a great night on the stage with:

Leonidas Paterakis (@FablabVenezia) 
from FabLab, Venice

Lorenzo Speranzoni (@inserpio)
from LARUS, Venezia

Fabiana Zollo (@zollofab)
from Università Cà Foscari, Venezia

Please REGISTER as soon as possible so we can provide enough beers (and bagigi) for you all! 

Beers are again VERY KINDLY PROVIDED BY ESTRELLA!

Thanks to LARUS for the technical support and logistics.See you there!

CheersAlice, Fabio, Marco, Nicola, Rocco
