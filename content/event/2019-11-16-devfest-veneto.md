---
title: 'DevFest Veneto 2019'
date: 2019-11-15T09:30:00.000Z
talk: DevFest
slot:
  end: 2019-11-16T07:00:00.000Z
  group: []
  location: Via Torino, Campus Scientifico
  start: 2019-11-16T09:30:00.000Z
  url: 'https://devfestvenice.com/'
---

Brace Yourself, DevFest Veneto 19 Is Coming!

Yes, we are proud to announce that also this year the DevFest Veneto will take place on 16th November! But we have lots of news!

DevFests are community-led developer events hosted by Google Developer Groups around the globe. These are events with talks, workshops and networking moments dedicated to professional developers and designers, entrepreneurs, students or simple enthusiasts. It is an opportunity to share skills and passions within the universe of Google technologies (and not only).

This year, we have committed to switching the conference main language from Italian to English, to host speakers and attendees from all over the world. So this year, the majority of the sessions will be in English (all the language information will be available on the schedule section of the website: https://devfestvenice.com/schedule/).

And we have also a new location! This year the DevFest will take place at "Campus Scientifico Università Ca' Foscari" - Via Torino, 155, 30170 Mestre, Venezia VE in the building "Zeta"

The event will last all day, from 9.00 am to 6.00 pm. The schedule is available on the official website!
