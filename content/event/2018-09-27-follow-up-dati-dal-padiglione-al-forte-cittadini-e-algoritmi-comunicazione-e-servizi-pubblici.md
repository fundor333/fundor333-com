---
title: >-
  Follow UP! Dati dal Padiglione al Forte: Cittadini e algoritmi, comunicazione
  e servizi pubblici
date: 2018-09-27T16:30:20.094Z
talk: Follow Up
slot:
  end: 2018-09-27T20:37:20.102Z
  group: []
  location: Forte Marghera
  start: 2018-09-27T20:37:20.098Z
  url: >-
    https://www.eventbrite.it/e/biglietti-follow-up-dati-dal-padiglione-al-forte-cittadini-e-algoritmi-comunicazione-servizi-pubblici-e-50075741892#
---
Marco Goldin, il data scientist da sorriso maliardo, ci mostrerà degli algoritmi di cooccorrenza per il targeting delle comunicazioni comunali ai cittadini. DOMANDA: come si possono gestire algoritmicamente i dati delle interazioni dei cittadini? Sembra una cosa supertecnica ma serve per proporre i servizi ai cittadini ottimizzando la comunicazione con l'Amministrazione nei vari canali, che sia il sito web, i social, URP. Esempio banale: grazie all'analisi di cooccorrenza, se il cittadino 1 usa il servizio A e il cittadino 2 usa il servizio A e quello B, le macchine automaticamente propongono il servizio B anche al cittadino 1. Si farà un'analisi di qualche struttura di sito web e una serie di considerazioni.
