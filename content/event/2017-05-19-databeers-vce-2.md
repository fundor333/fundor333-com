---
title: DataBeers VCE 2
talk: Tre presentazioni da speacker di vario tipo
slot:
  start: 2017-05-19 00:00:00 +0000
  end: 2017-05-19 00:00:00 +0000
  url: https://databeersvce.tumblr.com/post/160223833870/databeers-venezia-2-190517-negozio-piave-67
  group:
  - DataBeers Venezia
  location: Negozio Piave 67, Via Piave 67, Venezia

---
Hey Data Lovers! Following the huge success of the first edtion… we’re back! Databeers Venice #2 is live! We’re waiting for you next Friday, May 19th @ Negozio Piave 67, via Piave 67, Venezia, starting at 18:00 sharp!

Five stunning speakers will drive the scene for you:

---
Marco De Nadai
Dottorando di Ricerca, Università di Trento
Cosa accomuna Bogotà e Los Angeles? Il crimine raccontato e predetto dalle caratteristiche urbane.

Luca Salmasi
Università di Perugia
Stima degli effetti di trattamento da cartelle cliniche elettroniche: metodi e potenziali impatti.

Emanuele Chiericato
Imprenditore Digitale
Come strutturare una strategia di lead generation per la propria azienda.

Bruno Anastasia e Maurizio Gambuzza
Veneto Lavoro
Navigare i numeri sull’occupazione

---
As usual, please REGISTER as soon as possible so we can provide enough beers (and bagigi) for you all! 

Beers are again VERY KINDLY PROVIDED BY ESTRELLA!

Thanks to LARUS for the technical and logistic support.

See you there!

Cheers

Alice, Fabio, Marco, Nicola, Rocco
