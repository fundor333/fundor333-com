---
title: >-
  Venezia Conta, dai dati all'ambiente: Biennale Architettura Follow UP! Dal
  Padiglione Venezia a Forte Marghera
date: 2018-06-26T16:30:53.852Z
talk: Follow Up
slot:
  end: 2018-06-25T22:00:00.000Z
  group: []
  location: Forte Marghera
  start: 2018-06-26T20:33:53.865Z
  url: >-
    https://www.eventbrite.it/e/biglietti-venezia-conta-dai-dati-allambiente-biennale-architettura-follow-up-dal-padiglione-venezia-al-forte-47146147396#
---
Per il terzo incontro per il Padiglione Venezia parleremo di design della comunicazione per usare i dati per informare sull’emergenze ambientali e sociali nella città: strumenti interpretativi per affrontare la complessità.

Dal Corso di laurea in disegno industriale e multimedia del Dipartimento di progettazione e pianificazione in ambienti complessi, verranno proposte strategie e metodi per informare sull’ambiente naturale e civile nel Comune di Venezia. Gli unici strumenti saranno i numeri e dati rilevati sul territorio e "lavorati" per diventare strumenti informativi.



* Sergio Brugiolo (docente di design della comunicazione)
* Claudia Faraone (architetto urbanista)
* Stefano Giacomazzi (presidente Viva Piraghetto associazione culturale)
* Anna Saccani (designer della comunicazione)
* Luca Corsato (moderatore)
