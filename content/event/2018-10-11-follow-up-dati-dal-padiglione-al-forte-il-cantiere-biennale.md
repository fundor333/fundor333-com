---
title: 'Follow UP! Dati dal Padiglione al Forte: Il cantiere Biennale'
date: 2018-10-11T16:00:00.000Z
talk: Follow Up
slot:
  end: 2018-10-11T20:43:35.195Z
  group: []
  location: Forte Marghera
  start: 2018-10-11T20:43:35.191Z
  url: >-
    https://www.eventbrite.it/e/biglietti-follow-up-dati-dal-padiglione-al-forte-il-cantiere-biennale-51215275267#
---
La storia delle trasformazioni dei Giardini in luogo espositivo; la costruzione dei padiglioni nazionali; la "vita" delle mostre e degli allestimenti museali. Ne parleremo con:



* Francesca Castellani (Università Iuav di Venezia)
* Eleonora Charans (Università Iuav di Venezia)
* Chiara Di Stefano (Università per Stranieri di Perugia)
* Cristiano Guarneri (Università degli Studi di Padova)
* Stefano Zaggia (Università degli Studi di Padova)
* Guido Zucconi (Università Iuav di Venezia)
