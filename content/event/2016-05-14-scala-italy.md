---
title: Scala Italy
talk: The only Italian conference on Scala
slot:
  start: 2016-05-14 00:00:00 +0000
  end: 2016-05-14 00:00:00 +0000
  url: http://2016.scala-italy.it/
  group:
  - Scala Italy
  location: Ex Cotonificio Veneziano Santa Marta, Dorsoduro 2196, 30123 Venezia
  video: https://vimeo.com/channels/scalaitaly2016

---
Scala Italy is the third edition of the annual Italian conference on the Scala Programming Language.

It is brought to you by and for the scala community. All conference talks are peer evaluated by members of the community itself.
