---
title: "Sushi_go"
feature_image: "sushi_go.jpg"
slug: "sushi_go"
link: "https://gamewright.com/product/Sushi-Go"
---

A fast card game about sushi. You need to get the best sushi combination after 3 round of "tacking from the trail".
It's a fast game for 2 to 8 people. The duration of the game don't change with the numbers of player.
