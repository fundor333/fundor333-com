---
title: "now"
type: now

---

Right now I am working on:

* A django backend
* Bots with Telegram
* A sensor's project
* Some Docker work

This is a now page, and if you have your own site, [you should make one too](https://nownownow.com/about)

NB: I love reading and this is my goal for this year
